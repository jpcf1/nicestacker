#include <iostream>
#include <time.h>
#define ARMA_64BIT_WORD
#include <armadillo>

#define IMG_H 4024
#define IMG_W 6024
#define N_P 24240576
#define N_F 6
#define NUM_OP 10

using namespace std;
using namespace arma;

int main(void) {

	// Structs that keep time
	struct timespec t_start, t_end;
	long int dT_tran = 0;
	long int dT_mult = 0;
	long int dT_inv  = 0;

	// Sets the seed to random
	arma_rng::set_seed_random();

	// Declares matrices
	mat A    = mat(N_P, N_F, fill::randu);
	mat At   = mat(N_F, N_P, fill::randu);
	mat At_A = mat(N_F, N_F, fill::randu);
	mat invB = mat(N_F, N_F, fill::randu);
	mat B = mat(N_F, N_F, fill::randu);

	// Times a matrix transpose operation
	for(int i=0; i < NUM_OP; i++) {
		// Create a N_PxF random matrix and print it on the screen
		mat A  = mat(N_P, N_F, fill::randu);
	
		clock_gettime(CLOCK_MONOTONIC, &t_start);
		At = A.t();
		clock_gettime(CLOCK_MONOTONIC, &t_end);
		dT_tran += t_end.tv_nsec - t_start.tv_nsec;
	}
	cout << "Average time for Transposition: " << (double)dT_tran/NUM_OP/1000000.0 << "ms \n";

	// Times a matrix multiplication operation
	for(int i=0; i < NUM_OP; i++) {
		// Create a N_PxF random matrix and print it on the screen
		mat A  = mat(N_P, N_F, fill::randu);
	
		clock_gettime(CLOCK_MONOTONIC, &t_start);
		At_A = At * A;
		clock_gettime(CLOCK_MONOTONIC, &t_end);
		dT_mult += t_end.tv_nsec - t_start.tv_nsec;
	}
	cout << "Average time for Multiplication: " << (double)dT_mult/NUM_OP/1000000.0 << "ms \n";

	// Times a matrix inversion operation
	for(int i=0; i < NUM_OP; i++) {
		// Create a N_PxF random matrix and print it on the screen
		mat V = mat(N_F, N_F, fill::randu);
	
		clock_gettime(CLOCK_MONOTONIC, &t_start);
		invB = inv(B);
		clock_gettime(CLOCK_MONOTONIC, &t_end);
		dT_inv += t_end.tv_nsec - t_start.tv_nsec;
	}
	cout << "Average time for Inversion: " << (double)dT_inv/NUM_OP/1000000.0 << "ms \n";

	cout << "sizeof(A): " << sizeof(A) << "\n";
	cout << "sizeof(At): " << sizeof(At) << "\n";
	cout << "sizeof(At_A): " << sizeof(At_A) << "\n";
	cout << "sizeof(At_A): " << At_A << "\n";
	return 0;
}


