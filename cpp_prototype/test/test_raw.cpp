#include <iostream>
#include "libraw/libraw.h"

using namespace std;

int main(void) {

	// Creates a RAW processor
	LibRaw RawProc;

	// Opens an image
	RawProc.open_file("../../data/praia.dng");

	// Checks the size of the opened image
	cout << "Image size: " << RawProc.imgdata.sizes.width << " x " << RawProc.imgdata.sizes.height << endl;

	// Unpacking the image and demosaicing
	RawProc.unpack();
	RawProc.raw2image();
	

	return 0;
}


