#include <iostream>
#include <errno.h>
#include <opencv2/opencv.hpp>
#include "libraw/libraw.h"

using namespace std;

cv::Mat libraw2cv(libraw_processed_image_t* img_libraw) {
	cv::Mat img_cv = new cv::Mat(img_libraw->height, img_libraw->width, CV_8UC3);
	int rows     = img_libraw->height;
	int cols     = img_libraw->width;
	int num_chan = img_libraw->colors; 
	int num_pixels = rows * img_libraw->width;

	for(int c=0; c < num_chan; c++) {
		for(int i=0; i < num_pixels; i++) {
			img_cv.at<unsigned char>(i / cols, i % cols, c) = img_libraw->data[c+num_pixels*num_chan];
		}
	}

	return &img_cv;

}

int main(void) {

	// Creates a RAW processor and a structure that will hold the processed image
	LibRaw RawProc;
	libraw_processed_image_t* dcraw_output;

    // Creates an cv2 matrix for image storage
    cv::Mat Img;

	// Opens an image
	if(LIBRAW_SUCCESS != RawProc.open_file("../../data/praia.dng")) {
		cout << "There was an error opening the file\n";
		return -1;
	}

	// Checks the size of the opened image and other metadata
	cout << "Image size: " << RawProc.imgdata.sizes.width << " x " << RawProc.imgdata.sizes.height << endl;

	// Unpacking the image and demosaicing
    cout << "Developing RAW file..\n";
    if(LIBRAW_SUCCESS == RawProc.unpack()) {

    	if(LIBRAW_SUCCESS == RawProc.dcraw_process()) {

			dcraw_output = RawProc.dcraw_make_mem_image();

			if (LIBRAW_IMAGE_BITMAP == dcraw_output->type) {
				printf("Type is BMAP\nNum Channels: %d\nBits per chan: %d\n", dcraw_output->colors, dcraw_output->bits);
	        } else {
	        	fprintf(stderr, "There was an error making RGB image");
	        } 
	    }
	    else {
	    	// Checks for errors
    		cout << "There was an error demosaicing the RAW data: \n";
			return -1;
	    }
    }
    else {
		// Checks for errors
    	cout << "There was an error unpackig the RAW data\n";
		return -1;
    }
    cout << "Developing RAW file COMPLETED!\n";

	// Transforming raw file into Mat object that can be handled by openCV
    Img = cv::Mat(RawProc.imgdata.sizes.width, RawProc.imgdata.sizes.height, CV_8UC3, RawProc.imgdata.image);

    cv::imshow("Cenas", Img);
        
    cv::waitKey(0);
        
	return 0;
}


