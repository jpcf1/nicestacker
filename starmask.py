import numpy as np
import rawpy
import rawpy.enhance as rawpyen
import cv2
import time
from matplotlib import pyplot as plt

def processLine(img_in, img_out, win_sz, img_h, img_w, i, j):

	# Reset the variance and mean counters
	mean = 0.0
	M2 = 0.0
	variance = 0.0
	n = 0

	# For every pixel in the destination image, scan the original image in a window of size WINDOW_SIZE
	for x in range(-win_sz, win_sz, 1):
		for y in range(-win_sz, win_sz, 1):
			n += 1
			k = img_gs.item((i+x+win_sz, j+y+win_sz))
			delta  = k - mean
			mean  += delta/n
			M2 += delta*(k - mean)
	if(img_gs.item((i+win_sz, j+win_sz)) >  mean + 2.2*np.sqrt(M2/(n-1))):
		img_gs_work.itemset((i,j), 0)
	else:
		img_gs_work.itemset((i,j), 255)

	#print("Mean: ", mean, "Stddev: ", np.sqrt(M2/(n-1)), "Pixel Value: ", img_gs.item((i+win_sz)*img_w+j+win_sz), "N: ", n)

def computeLightPollutionGradient(learningStep, img_chan_in, img_h, img_w):
	W = np.random.random((6,1)) - 0.5
	
	for x_c in range(img_h):
		for y_c in range(img_w):
			val = img_chan_in.item((x_c,y_c))/255.0
			x = x_c/img_h
			y = y_c/img_w
			X = np.array([[1],[x],[y],[x*y],[x*x],[y*y]]);
			W = W + learningStep*(val - np.dot(W.T,X))*X;
			#print(W)

	for x_c in range(img_h):
		for y_c in range(img_w):
			val = img_chan_in.item((x_c,y_c))/255.0
			x = x_c/img_h
			y = y_c/img_w
			X = np.array([[1],[x],[y],[x*y],[x*x],[y*y]]);
			W = W + learningStep*(val - np.dot(W.T,X))*X;
			#print(W)

	for x_c in range(img_h):
		for y_c in range(img_w):
			val = img_chan_in.item((x_c,y_c))/255.0
			x = x_c/img_h
			y = y_c/img_w
			X = np.array([[1],[x],[y],[x*y],[x*x],[y*y]]);
			W = W + learningStep*(val - np.dot(W.T,X))*X;
			#print(W)

	for x_c in range(img_h):
		for y_c in range(img_w):
			val = img_chan_in.item((x_c,y_c))/255.0
			x = x_c/img_h
			y = y_c/img_w
			X = np.array([[1],[x],[y],[x*y],[x*x],[y*y]]);
			W = W + learningStep*(val - np.dot(W.T,X))*X;
			#print(W)

	for x_c in range(img_h):
		for y_c in range(img_w):
			val = img_chan_in.item((x_c,y_c))/255.0
			x = x_c/img_h
			y = y_c/img_w
			X = np.array([[1],[x],[y],[x*y],[x*x],[y*y]]);
			W = W + learningStep*(val - np.dot(W.T,X))*X;
			#print(W)

	return W;

def printLightPollutionGradient(W, img_h, img_w):
	img_out = np.zeros((img_h, img_w));

	for x_c in range(img_h):
		for y_c in range(img_w):
			x = x_c/img_h
			y = y_c/img_w
			img_out.itemset((x_c,y_c), 255*np.dot(np.array([[1,x,y,x*y,x*x,y*y]]),W))

	return img_out

# Parameters
WINDOW_SIZE = 30
NUM_PATCHES = 128

# Reads rawfile and converts it to RGB array
filename = ['praia.dng']
img_raw = rawpy.imread(filename[0])
img_rgb = img_raw.postprocess(output_color = rawpy.ColorSpace.sRGB)
img_rgb = img_rgb[2500:3000, 2500:3000] # slicing to make processing faster

#img_rgb = cv2.imread('praia.dng')
img_gs_wth = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2GRAY)
img_h = img_gs_wth.shape[0]
img_w = img_gs_wth.shape[1]
img_gs_work = np.zeros(img_gs_wth.shape)
img_gs = cv2.copyMakeBorder(img_gs_wth, WINDOW_SIZE, WINDOW_SIZE, WINDOW_SIZE, WINDOW_SIZE, cv2.BORDER_REFLECT)




# Computes the Light Pollution gradient and plots it
W_r = computeLightPollutionGradient(0.05, img_gs_wth, img_h, img_w);
img_lp = printLightPollutionGradient(W_r, img_h, img_w)

plt.figure(1)
plt.subplot(211)
plt.imshow(img_gs_wth, cmap='gray')
plt.subplot(212)
plt.imshow(img_lp, cmap='gray')
plt.figure(2)
plt.imshow(img_gs_wth-img_lp, cmap='gray')
plt.show()

"""
# Computes the Star Mask
for i in range(img_h):

	start_t = time.time()

	for j in range(img_w):
		processLine(img_gs, img_gs_work, WINDOW_SIZE, img_h, img_w, i, j)

	print("Line: ", i, "processed in ", time.time() - start_t, "seconds")

plt.figure(1)
plt.subplot(211)
plt.imshow(img_gs_wth, cmap='gray')
plt.subplot(212)
plt.imshow(img_gs_work, cmap='gray')
plt.show()
"""
