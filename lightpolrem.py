import numpy as np
import rawpy
import rawpy.enhance as rawpyen
import cv2
import time
from matplotlib import pyplot as plt

def computeFeatureMatrix(img_h, img_w):
	# Create feature matrix
	X = np.zeros((img_h*img_w,6))

	for x in range(img_h):
		for y in range(img_w):
			X[x*img_w+y][0] = 1;
			X[x*img_w+y][1] = x;
			X[x*img_w+y][2] = y;
			X[x*img_w+y][3] = x*y;
			X[x*img_w+y][4] = x**2;
			X[x*img_w+y][5] = y**2;

	return X

def computeLightPollutionGradient(X, img_chan_in, img_h, img_w):
	W = np.zeros((6,1))

	Y = np.zeros((img_h*img_w,1))

	for x in range(img_h):
		for y in range(img_w):
			Y[x*img_w+y,0] = img_chan_in[x,y];
	
	to_be_inv = np.dot(X.T,X);
	print("Multiplied Xt by X");

	mat_inv = np.linalg.inv(to_be_inv);
	print("Matrix Inverted");

	mat_mult_1 = np.dot(mat_inv,X.T);
	print("Matrix multilpied");

	W = np.dot(mat_mult_1, Y);
	print("Feature vec: ", W);

	return W;

def printLightPollutionGradient(X, W, corr_intensity, img_h, img_w):

	Y = np.round(corr_intensity*np.dot(X,W)).astype(np.uint8);

	return Y.reshape((img_h, img_w))

# Parameters
WINDOW_SIZE = 30
NUM_PATCHES = 128

# Reads rawfile and converts it to RGB array
filename = ['praia.dng']
img_raw = rawpy.imread(filename[0])
img_rgb = img_raw.postprocess(output_color = rawpy.ColorSpace.sRGB)
img_rgb = img_rgb # slicing to make processing faster

#img_rgb = cv2.imread('praia.dng')
img_h  = img_rgb.shape[0]
img_w  = img_rgb.shape[1]
img_lp = np.zeros(img_rgb.shape, dtype=np.uint8)




# Computes the Light Pollution gradient and plots it
X = computeFeatureMatrix(img_h, img_w)
time_init = time.time()
W_R = computeLightPollutionGradient(X, img_rgb[:,:,0], img_h, img_w);
print("Patch of ", img_h, "x", img_w, " RED processed in ", time.time() - time_init," seconds");

time_init = time.time()
W_G = computeLightPollutionGradient(X, img_rgb[:,:,1], img_h, img_w);
print("Patch of ", img_h, "x", img_w, " GREEN processed in ", time.time() - time_init," seconds");

time_init = time.time()
W_B = computeLightPollutionGradient(X, img_rgb[:,:,2], img_h, img_w);
print("Patch of ", img_h, "x", img_w, " BLUE processed in ", time.time() - time_init," seconds");


img_lp[:,:,0] = printLightPollutionGradient(X, W_R, 0.95, img_h, img_w)
img_lp[:,:,1] = printLightPollutionGradient(X, W_G, 0.95, img_h, img_w)
img_lp[:,:,2] = printLightPollutionGradient(X, W_B, 0.95, img_h, img_w)

plt.figure(1)
plt.subplot(211)
plt.imshow(img_rgb)
plt.subplot(212)
plt.imshow(img_lp)
plt.figure(2)
a = img_rgb.astype(np.int8)-img_lp.astype(np.int8)
a[a < 0] = 0
plt.imshow(a.astype(np.uint8))
plt.show()

"""
	for x_c in range(img_h):
		for y_c in range(img_w):
			val = img_chan_in.item((x_c,y_c))/255.0
			x = x_c/img_h
			y = y_c/img_w
			X = np.array([[1],[x],[y],[x*y],[x*x],[y*y]]);
			W = W + learningStep*(val - np.dot(W.T,X))*X;
			#print(W)
"""

